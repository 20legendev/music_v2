<?php

/*
 * This file is part of the Predis package.
 *
 * (c) Daniele Alessandri <suppakilla@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Zend\Predis\Profile;

/**
 * Server profile for Redis 2.0.
 *
 * @author Daniele Alessandri <suppakilla@gmail.com>
 */
class RedisVersion200 extends RedisProfile
{
    /**
     * {@inheritdoc}
     */
    public function getVersion()
    {
        return '2.0';
    }

    /**
     * {@inheritdoc}
     */
    public function getSupportedCommands()
    {
        return array(
            /* ---------------- Redis 1.2 ---------------- */

            /* commands operating on the key space */
            'EXISTS'                    => 'Zend\Predis\Command\KeyExists',
            'DEL'                       => 'Zend\Predis\Command\KeyDelete',
            'TYPE'                      => 'Zend\Predis\Command\KeyType',
            'KEYS'                      => 'Zend\Predis\Command\KeyKeys',
            'RANDOMKEY'                 => 'Zend\Predis\Command\KeyRandom',
            'RENAME'                    => 'Zend\Predis\Command\KeyRename',
            'RENAMENX'                  => 'Zend\Predis\Command\KeyRenamePreserve',
            'EXPIRE'                    => 'Zend\Predis\Command\KeyExpire',
            'EXPIREAT'                  => 'Zend\Predis\Command\KeyExpireAt',
            'TTL'                       => 'Zend\Predis\Command\KeyTimeToLive',
            'MOVE'                      => 'Zend\Predis\Command\KeyMove',
            'SORT'                      => 'Zend\Predis\Command\KeySort',

            /* commands operating on string values */
            'SET'                       => 'Zend\Predis\Command\StringSet',
            'SETNX'                     => 'Zend\Predis\Command\StringSetPreserve',
            'MSET'                      => 'Zend\Predis\Command\StringSetMultiple',
            'MSETNX'                    => 'Zend\Predis\Command\StringSetMultiplePreserve',
            'GET'                       => 'Zend\Predis\Command\StringGet',
            'MGET'                      => 'Zend\Predis\Command\StringGetMultiple',
            'GETSET'                    => 'Zend\Predis\Command\StringGetSet',
            'INCR'                      => 'Zend\Predis\Command\StringIncrement',
            'INCRBY'                    => 'Zend\Predis\Command\StringIncrementBy',
            'DECR'                      => 'Zend\Predis\Command\StringDecrement',
            'DECRBY'                    => 'Zend\Predis\Command\StringDecrementBy',

            /* commands operating on lists */
            'RPUSH'                     => 'Zend\Predis\Command\ListPushTail',
            'LPUSH'                     => 'Zend\Predis\Command\ListPushHead',
            'LLEN'                      => 'Zend\Predis\Command\ListLength',
            'LRANGE'                    => 'Zend\Predis\Command\ListRange',
            'LTRIM'                     => 'Zend\Predis\Command\ListTrim',
            'LINDEX'                    => 'Zend\Predis\Command\ListIndex',
            'LSET'                      => 'Zend\Predis\Command\ListSet',
            'LREM'                      => 'Zend\Predis\Command\ListRemove',
            'LPOP'                      => 'Zend\Predis\Command\ListPopFirst',
            'RPOP'                      => 'Zend\Predis\Command\ListPopLast',
            'RPOPLPUSH'                 => 'Zend\Predis\Command\ListPopLastPushHead',

            /* commands operating on sets */
            'SADD'                      => 'Zend\Predis\Command\SetAdd',
            'SREM'                      => 'Zend\Predis\Command\SetRemove',
            'SPOP'                      => 'Zend\Predis\Command\SetPop',
            'SMOVE'                     => 'Zend\Predis\Command\SetMove',
            'SCARD'                     => 'Zend\Predis\Command\SetCardinality',
            'SISMEMBER'                 => 'Zend\Predis\Command\SetIsMember',
            'SINTER'                    => 'Zend\Predis\Command\SetIntersection',
            'SINTERSTORE'               => 'Zend\Predis\Command\SetIntersectionStore',
            'SUNION'                    => 'Zend\Predis\Command\SetUnion',
            'SUNIONSTORE'               => 'Zend\Predis\Command\SetUnionStore',
            'SDIFF'                     => 'Zend\Predis\Command\SetDifference',
            'SDIFFSTORE'                => 'Zend\Predis\Command\SetDifferenceStore',
            'SMEMBERS'                  => 'Zend\Predis\Command\SetMembers',
            'SRANDMEMBER'               => 'Zend\Predis\Command\SetRandomMember',

            /* commands operating on sorted sets */
            'ZADD'                      => 'Zend\Predis\Command\ZSetAdd',
            'ZINCRBY'                   => 'Zend\Predis\Command\ZSetIncrementBy',
            'ZREM'                      => 'Zend\Predis\Command\ZSetRemove',
            'ZRANGE'                    => 'Zend\Predis\Command\ZSetRange',
            'ZREVRANGE'                 => 'Zend\Predis\Command\ZSetReverseRange',
            'ZRANGEBYSCORE'             => 'Zend\Predis\Command\ZSetRangeByScore',
            'ZCARD'                     => 'Zend\Predis\Command\ZSetCardinality',
            'ZSCORE'                    => 'Zend\Predis\Command\ZSetScore',
            'ZREMRANGEBYSCORE'          => 'Zend\Predis\Command\ZSetRemoveRangeByScore',

            /* connection related commands */
            'PING'                      => 'Zend\Predis\Command\ConnectionPing',
            'AUTH'                      => 'Zend\Predis\Command\ConnectionAuth',
            'SELECT'                    => 'Zend\Predis\Command\ConnectionSelect',
            'ECHO'                      => 'Zend\Predis\Command\ConnectionEcho',
            'QUIT'                      => 'Zend\Predis\Command\ConnectionQuit',

            /* remote server control commands */
            'INFO'                      => 'Zend\Predis\Command\ServerInfo',
            'SLAVEOF'                   => 'Zend\Predis\Command\ServerSlaveOf',
            'MONITOR'                   => 'Zend\Predis\Command\ServerMonitor',
            'DBSIZE'                    => 'Zend\Predis\Command\ServerDatabaseSize',
            'FLUSHDB'                   => 'Zend\Predis\Command\ServerFlushDatabase',
            'FLUSHALL'                  => 'Zend\Predis\Command\ServerFlushAll',
            'SAVE'                      => 'Zend\Predis\Command\ServerSave',
            'BGSAVE'                    => 'Zend\Predis\Command\ServerBackgroundSave',
            'LASTSAVE'                  => 'Zend\Predis\Command\ServerLastSave',
            'SHUTDOWN'                  => 'Zend\Predis\Command\ServerShutdown',
            'BGREWRITEAOF'              => 'Zend\Predis\Command\ServerBackgroundRewriteAOF',

            /* ---------------- Redis 2.0 ---------------- */

            /* commands operating on string values */
            'SETEX'                     => 'Zend\Predis\Command\StringSetExpire',
            'APPEND'                    => 'Zend\Predis\Command\StringAppend',
            'SUBSTR'                    => 'Zend\Predis\Command\StringSubstr',

            /* commands operating on lists */
            'BLPOP'                     => 'Zend\Predis\Command\ListPopFirstBlocking',
            'BRPOP'                     => 'Zend\Predis\Command\ListPopLastBlocking',

            /* commands operating on sorted sets */
            'ZUNIONSTORE'               => 'Zend\Predis\Command\ZSetUnionStore',
            'ZINTERSTORE'               => 'Zend\Predis\Command\ZSetIntersectionStore',
            'ZCOUNT'                    => 'Zend\Predis\Command\ZSetCount',
            'ZRANK'                     => 'Zend\Predis\Command\ZSetRank',
            'ZREVRANK'                  => 'Zend\Predis\Command\ZSetReverseRank',
            'ZREMRANGEBYRANK'           => 'Zend\Predis\Command\ZSetRemoveRangeByRank',

            /* commands operating on hashes */
            'HSET'                      => 'Zend\Predis\Command\HashSet',
            'HSETNX'                    => 'Zend\Predis\Command\HashSetPreserve',
            'HMSET'                     => 'Zend\Predis\Command\HashSetMultiple',
            'HINCRBY'                   => 'Zend\Predis\Command\HashIncrementBy',
            'HGET'                      => 'Zend\Predis\Command\HashGet',
            'HMGET'                     => 'Zend\Predis\Command\HashGetMultiple',
            'HDEL'                      => 'Zend\Predis\Command\HashDelete',
            'HEXISTS'                   => 'Zend\Predis\Command\HashExists',
            'HLEN'                      => 'Zend\Predis\Command\HashLength',
            'HKEYS'                     => 'Zend\Predis\Command\HashKeys',
            'HVALS'                     => 'Zend\Predis\Command\HashValues',
            'HGETALL'                   => 'Zend\Predis\Command\HashGetAll',

            /* transactions */
            'MULTI'                     => 'Zend\Predis\Command\TransactionMulti',
            'EXEC'                      => 'Zend\Predis\Command\TransactionExec',
            'DISCARD'                   => 'Zend\Predis\Command\TransactionDiscard',

            /* publish - subscribe */
            'SUBSCRIBE'                 => 'Zend\Predis\Command\PubSubSubscribe',
            'UNSUBSCRIBE'               => 'Zend\Predis\Command\PubSubUnsubscribe',
            'PSUBSCRIBE'                => 'Zend\Predis\Command\PubSubSubscribeByPattern',
            'PUNSUBSCRIBE'              => 'Zend\Predis\Command\PubSubUnsubscribeByPattern',
            'PUBLISH'                   => 'Zend\Predis\Command\PubSubPublish',

            /* remote server control commands */
            'CONFIG'                    => 'Zend\Predis\Command\ServerConfig',
        );
    }
}
