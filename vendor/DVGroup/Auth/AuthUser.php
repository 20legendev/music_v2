<?php

namespace DVGroup\Auth;

use Zend\Session\Container;

class AuthUser
{
  public $session;

  public function __construct()
  {
    $this->session = new Container("USER");
  }

  public function isAuthen()
  {
    if ($this->session->offsetExists('username')) {
      return TRUE;
    }
    return FALSE;
  }

  public function isUser($username)
  {
    if ($this->session->username !== $username) {
      return TRUE;
    }
    return FALSE;
  }

  public function getGroup()
  {
    return $this->session->group;
  }

  public function getUsername()
  {
    return $this->session->username;
  }

  public function getUser()
  {
    return unserialize($this->session->user);
  }
  public function destroy()
  {
    $this->session->offsetUnset('username');
    $this->session->offsetUnset('group');
  }

}