<?php 
namespace DVGroup\Neo4j;
use Everyman\Neo4j\Client;
class Neo4j extends Client
{
    public function __construct()
    {
        $sv=$this->getConfig()['neo4j-sv'];
        parent::__construct($sv['host']);
    }
    protected function getConfig()
     {
        //echo __DIR__ . '/../config/autoload/global.php';
         return include __DIR__ . '/../../../config/autoload/global.php';
     }
}
