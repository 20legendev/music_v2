<?php

namespace DVGroup\Db\Model;

use DVGroup\Redis\Redis;
use DVGroup\Common\CommonLibs;
use Zend\ServiceManager\ServiceManager;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\HydratingResultSet;

class BaseTable {
	protected $tableGateway;
	protected $redis;
	protected $neo4j;

    public function __construct(TableGateway $tableGateway = null) {
        if($tableGateway){
            $this->tableGateway = $tableGateway;
        }
	}

	public function getRedis() {
        if(!$this->redis){
            $this->redis = new Redis();
        }
		return $this -> redis;
	}

	public function getLastInsertId() {
		return $this -> tableGateway -> lastInsertValue;
	}

	protected function getNeo4jSv() {
		if (!$this -> neo4j) {
			$this -> neo4j = $this -> sm -> get('neo4j');
		}
		return $this -> neo4j;
	}

	public function getNeo4j() {
		return $this -> neo4j;
	}

	public function exchangeArray($data) {
		foreach ($data as $key => $value) {
			$this -> $key = (isset($data[$key])) ? $value : null;
		}
	}
	
	public function getCacheableData($query, $cache_time = null){
		$sql = new \Zend\Db\Sql\Sql($this->tableGateway->getAdapter());
		$query_string = $sql->getSqlStringForSqlObject($query);
		$key = '_CACHE:AUTO_CACHE:' . md5($query_string);
		if($this->getRedis()->exists($key)){
			return $this->_get($key);
		}
		$result = $this->tableGateway->getAdapter()->query($query_string	);
        $result = $result->execute();
        
		$result_arr = array();
		foreach($result as $value){
			$result_arr[] = $value;
		};
        if(!$cache_time){
            $config = $this->getConfig();
            $cache_time = $config['CACHE_TIME']['SHORT'];
        }
		$this->_set($key, $result_arr, $cache_time);
		// echo "TO_DB " . $query_string . "<br>";
		return $result_arr;
	}
    
    
    public function _get($key) {
		$redis = $this->getRedis();
      	$data = $redis->get($key);
      	if(!$data){
        	return NULL;
      	}
      	return CommonLibs::UnZip($data);
  	}

  	public function _set($key, $value, $expire = 7200) {
    	$redis = $this->getRedis();
      	if ($expire > 0) {
       		return  $redis->set($key, CommonLibs::Zip($value), $expire);
      	} else {
       		return  $redis->set($key, CommonLibs::Zip($value));
      	}
  	}
    
    protected function getConfig() {
         return include __DIR__ . '/../../../../config/autoload/global.php';
    }

}
