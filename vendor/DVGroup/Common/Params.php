<?php

namespace DVGroup\Common;

class Params {

    public static function getParams($param = null) {
        $arrParams = self::getConfig();
        if(isset($param) && !is_null($param)){
            return $arrParams[$param];
        }
    }

    public static function getConfig()
    {
        return include __DIR__ . '/../../../config/autoload/params.php';
    }
}