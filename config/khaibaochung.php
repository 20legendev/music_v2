<?php
    /**
     * Created by PhpStorm.
     * User: Administrator
     * Date: 10/15/14
     * Time: 10:19 AM
     */


//===Cate Module===================================================================================================
    // cua ai ten gi
    'All_cat' => ['type'    => 'segment',
                  'options' => ['route'    => '/cate.html',
                                'defaults' => ['controller' => 'Cate\Controller\Index',
                                               'action'     => 'index']]],
        //catelogy bai hat-------------
            'ChuyenMuc' => ['type'    => 'segment',
                            'options' => ['route'       => '/chuyen-muc[/:slug][/:sort].html',
                                          'constraints' => ['slug' => '[a-zA-Z0-9_-]*',
                                                            'sort' => '[a-zA-Z][a-zA-Z0-9_-]*'],
                                          'defaults'    => ['controller' => 'Cate\Controller\Categories',
                                                            'action'     => 'show']]],
        //catelogy video-------------
            'Videos' => ['type'    => 'segment',
                         'options' => ['route'       => '/videos[/:slug][/:sort].html',
                                       'constraints' => ['slug' => '[a-zA-Z0-9_-]*',
                                                         'sort' => '[a-zA-Z][a-zA-Z0-9_-]*'],
                                       'defaults'    => ['controller' => 'Cate\Controller\Categories',
                                                         'action'     => 'video']]],
        //xem bai hat------------------
            'Music' => ['type'    => 'segment',
                        'options' => ['route'       => '/bai-hat/[:slug].[:identify].html',
                                      'constraints' => ['slug'     => '[a-zA-Z0-9_-]*',
                                                        'identify' => '[a-zA-Z0-9_-]*'],
                                      'defaults'    => ['controller' => 'Cate\Controller\Music',
                                                        'action'     => 'music']]],
        //xem video------------------------
            'Video' => ['type'    => 'segment',
                        'options' => ['route'       => '/video/[:slug].[:identify].html',
                                      'constraints' => ['slug'     => '[a-zA-Z0-9_-]*',
                                                        'identify' => '[a-zA-Z0-9_-]*'],
                                      'defaults'    => ['controller' => 'Cate\Controller\Music',
                                                        'action'     => 'video']]],
        //xem video------------------------
          'Artist' => ['type'    => 'segment',
                             'options' => [ 'route'         => '/the-loai-nghe-si[/:category_slug][.:country_slug][/:page].html', 
                                            'constraints'   =>['country_slug'   => '[a-zA-Z0-9_-]*',
                                                               'category_slug'   =>'[a-zA-Z][a-zA-Z0-9_-]*',
                                                               'page'     => '[0-9]*'
                                                               ],   
                                            'defaults'      => ['controller' => 'Cate\Controller\Categories',
                                                                'action'     => 'artist']]],
        // nghệ sĩ-------------------------
            'NS' => array(
                'type'    => 'Segment',
                'options' => array(
                    'route'         => '/nghe-si[/:name]',
                    'constraints' => array(
                        'name'     => '[a-zA-Z0-9_-]*'
                    ),
                    'defaults'    => array(
                        '__NAMESPACE__' => 'Artist\Controller',
                        'controller'    => 'Artist\Controller\Index',
                        'action'        => 'index',
                    )
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'song' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '[.bai-hat][.:sort][/:page].html',
                            'constraints' => array(
                                'sort' => '(hot|new|like)',
                                'page' => '[0-9]*'
                            ),
                            'defaults' => array(
                                'action' => 'song'
                            ),
                        ),
                    ),
                ),
            ),
    //xem playlist

            'Playlist' => ['type'    => 'segment',
                           'options' => ['route'       => '/playlist/[:slug][.:identify].html',
                                         'constraints' => ['slug'     => '[a-zA-Z0-9_-]*',
                                                           'identify' => '[a-zA-Z0-9_-]*'],
                                         'defaults'    => ['controller' => 'Cate\Controller\Playlist',
                                                           'action'     => 'playlist']]],
    //trang chung playlist
            'PlaylistIndex' => ['type'    => 'segment',
                                'options' => ['route'       => '/playlist[.:sort].html',
                                              'constraints' => ['sort' => '[a-zA-Z0-9_-]*'],
                                              'defaults'    => ['controller' => 'Cate\Controller\Playlist',
                                                                'action'     => 'index']]],
    //HAIHS START Subject Module====================================================

       'subject'     => array(
            'type'    => 'Segment',
            'options' => array(
                'route'       => '/chu-de[.html]',
                'constraints' => array(
                    'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                    'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                    // 'page' => '[1-9][0-9]*'
                ),
                'defaults'    => array(
                    '__NAMESPACE__' => 'Subject\Controller',
                    'controller'    => 'Subject\Controller\Index',
                    'action'        => 'index',
                    //'page' => '1'
                )
            )
        ),
         'detail'      => array(
            'type'    => 'Segment',
            'options' => array(
                'route'       => '/chu-de[/:slug][/page=:page]',
                'constraints' => array(
                    'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                    'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                    'slug'       => '[a-zA-Z][a-zA-Z0-9-]*',
                    'page'       => '[1-9][0-9]*'
                ),
                'defaults'    => array(
                    '__NAMESPACE__' => 'Subject\Controller',
                    'controller'    => 'detail',
                    'action'        => 'index',
                    'page'          => '1'
                )
            )
        ),
         'subjectplaylist'    => array(
            'type'    => 'Segment',
            'options' => array(
                'route'       => '/playlist[/:slug]',
                'constraints' => array(
                    'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                    'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                    'slug'       => '[a-zA-Z][a-zA-Z0-9-]*',
                    'page'       => '[1-9][0-9]*'
                ),
                'defaults'    => array(
                    '__NAMESPACE__' => 'Subject\Controller',
                    'controller'    => 'playlist',
                    'action'        => 'index',
                    'page'          => '1'
                )
            )
        ),
       'subjectHot'  => array(
            'type'    => 'Segment',
            'options' => array(
                'route'       => '/chu-de-hot[/page=:page]',
                'constraints' => array(
                    'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                    'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                    'page'       => '[1-9][0-9]*'
                ),
                'defaults'    => array(
                    '__NAMESPACE__' => 'Subject\Controller',
                    'controller'    => 'subject-hot',
                    'action'        => 'index',
                    'page'          => '1',
                )
            )
        ),
        'subjectUser' => array(
            'type'    => 'Segment',
            'options' => array(
                'route'       => '/chu-de-cua-ban[/page=:page]',
                'constraints' => array(
                    'controller' => '[a-zA-Z][a-zA-Z0-9-]*',
                    'action'     => '[a-zA-Z][a-zA-Z0-9-]*',
                    'page'       => '[1-9][0-9]*'
                ),
                'defaults'    => array(
                    '__NAMESPACE__' => 'Subject\Controller',
                    'controller'    => 'subject-user',
                    'action'        => 'index',
                    'page'          => '1',
                )
            )
        ),
    //end START++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    
            
            'ArtistDetail' => array (
            		'type' => 'Segment',
            		'options' => array (
            				'route' => '/nghe-si[/:name]',
            				'constraints' => array (
            						'type' => '(abums|song)',
            						'name' => '[a-zA-Z0-9_-]*',
            						'page' => '[0-9]*'
            				),
            				'defaults' => array (
            						'controller' => 'Artist\Controller\Index',
            						'action' => 'index'
            				)
            		),
            		'may_terminate' => true,
            		'child_routes' => array (
            				'song' => array (
            						'type' => 'segment',
            						'options' => array (
            								'route' => '/bai-hat[.trang-:page].html',
            								'constraints' => array (
            										'page' => '[0-9]*'
            								),
            								'defaults' => array (
            										'controller' => 'Artist\Controller\Index',
            										'action' => 'song'
            								)
            						)
            				),
            				'album' => array (
            						'type' => 'segment',
            						'options' => array (
            								'route' => '/album[.trang-:page].html',
            								'constraints' => array (
            										'page' => '[0-9]*'
            								),
            								'defaults' => array (
            										'controller' => 'Artist\Controller\Index',
            										'action' => 'album'
            								)
            						)
            				),
            				'video' => array (
            						'type' => 'segment',
            						'options' => array (
            								'route' => '/video-clip[.trang-:page].html',
            								'constraints' => array (
            										'page' => '[0-9]*'
            								),
            								'defaults' => array (
            										'controller' => 'Artist\Controller\Index',
            										'action' => 'video'
            								)
            						)
            				),
            				'news' => array (
            						'type' => 'segment',
            						'options' => array (
            								'route' => '/tin-tuc[.trang-:page].html',
            								'constraints' => array (
            										'page' => '[0-9]*'
            								),
            								'defaults' => array (
            										'controller' => 'Artist\Controller\Index',
            										'action' => 'news'
            								)
            						)
            				)
            		)
            )
            