<?php

    return [
        'USER_SCALE'  => [0 => 0.4, 1 => 0.6, 2 => 0.6],
        'USER_FACTOR' => [0 => 1, 1 => 2, 2 => 1],
    ];