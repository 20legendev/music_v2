<?php
    /**
     * Global Configuration Override
     * You can use this file for overriding configuration values from modules, etc.
     * You would place values in here that are agnostic to the environment and not
     * sensitive to security.
     * @NOTE: In practice, this file will typically be INCLUDED in your source
     * control, so do not include passwords or other sensitive information in this
     * file.
     */

    return array(
        'redis-sv'        => ['sv1'=>['scheme'   => 'tcp',
                                    'db' => 0,
                                    'port'     => 6388,
                                    'host'     => '172.16.0.13'],
                            'sv2'=>['scheme'   => 'tcp',
                                    'db' => 1,
                                    'port'     => 6388,
                                    'host'     => '172.16.0.13']
,
                            ],
        'neo4j-sv'        => array(
            'port' => 7474,
            'host' => '172.16.0.13',
        ),
//        '-sv' => array(
//            'port' => 7474,
//            'host' => '172.16.0.13',
//        ),
        'db'              => array(
            'driver'         => 'Pdo',
            'dsn'            => 'mysql:dbname=music;host=172.16.0.13',
            'driver_options' => array(
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
            ),
        ),
        'service_manager' => array(
            'factories' => array(
                'Zend\Db\Adapter\Adapter'
                => 'Zend\Db\Adapter\AdapterServiceFactory',
            ),

        ),
        'CACHE_TIME'=>array(
			'SHORT'=>7200, // 2 gio
			'MEDIUM'=>86400, // 1 ngay
			'LONG'=>604800, // 7 ngay
			'FOREVER'=>2592000 // 1 thang
		),
		'HOST_FILE_MUSIC' => array(
			'base_url' => 'http://video.kenhtin.net/'
		)
    );
