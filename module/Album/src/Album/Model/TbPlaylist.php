<?php
namespace Album\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql; 
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use DVGroup\Db\Model\BaseTable;

class TbPlaylist extends BaseTable{
    	
    public function getAllPlaylist($sort_by='id',$sort='DESC')
    {
        $select=$this->tableGateway->getSql()->select();
        $select->columns(array('cate_id','name_search', 'title', 'thumnail', 'thumnail_goc', 'slug', 'identify')); 
             

        $select->order($sort_by.' '.$sort);

       $kk=$this->tableGateway->selectWith($select);
       $resultSet = new ResultSet();
       $resultSet->initialize($kk);
       $resultSet->buffer();
       return $resultSet;
    }
    public function getPlaylistByCate($cate_slug,$sort_by='id',$sort='DESC')
    {
        $select=$this->tableGateway->getSql()->select();
        $select->columns(array('cate_id','name_search', 'title', 'thumnail', 'thumnail_goc', 'slug', 'identify')); 
        $select->join('tb_cate',$this->tableGateway->getTable().'.cate_id=tb_cate.id',['name'=>'name'],'left');
        $select->where(array('tb_cate.slug'=>$cate_slug));

        $select->order($this->tableGateway->getTable().'.'.$sort_by.' '.$sort);
        //echo $select->getSqlString();

       $kk=$this->tableGateway->selectWith($select);
       $resultSet = new ResultSet();
       $resultSet->initialize($kk);
       $resultSet->buffer();
       return $resultSet;
    }
    public function getDetailByIdentify($identify = NULL)
    {
       if(!$identify)return false;    

        $rowset=$this->tableGateway->select(array('identify'=>$identify));      
        return $rowset->current();
    }
    public function getRelatedPlaylist($cate_id,$limit=10)
    {
        $cate_id=(int)$cate_id;
        if(!$cate_id)return false;
        $select=$this->tableGateway->getSql()->select();        
        $select->where($cate_id);       
        //$select->order($order.' DESC');
        $select->limit($limit);
      //  $select->join('tb_artist',$this->tableGateway->getTable().'.artist_id=tb_artist.id',array('name_search'));        
        $select->order(new \Zend\Db\Sql\Expression('RAND()'));
        //echo $select->getSqlString();
        $kk= $this->tableGateway->selectWith($select); 
        $resultSet = new ResultSet();
        $resultSet->initialize($kk);
        $resultSet->buffer();
        return $resultSet;
    }
    public function getAlbumby_artist_id($artist_id,$order='id',$sort='DESC')
    {
        $artist_id=(int)$artist_id;
        if(!$artist_id)return false;
        $select=$this->tableGateway->getSql()->select();    
        $select->columns(array('cate_id','name_search', 'title', 'thumnail', 'thumnail_goc', 'slug', 'identify')); 
        $select->where(array('artist_id'=>$artist_id));       
        $select->order($order.' '.$sort);
        $kk= $this->tableGateway->selectWith($select); 
        $resultSet = new ResultSet();
        $resultSet->initialize($kk);
        $resultSet->buffer();
        return $resultSet;
    }
    public function updatePageView($id,$view)
    {
       return $this->tableGateway->update(array('view'=>$view),array('id'=>$id));
    }
    
    public function getAlbumByArtist($artist_id, $limit = 12, $page = 0, $order = 'id', $sort = 'DESC'){
    	$artist_id = (int)$artist_id;
        if(!$artist_id) 
        	return false;
        $select = $this->tableGateway->getSql()->select();
        $select->where(array(
        	'artist_id'=>$artist_id
		));       
        $select->order($order . ' ' . $sort);
        $select->limit($limit);
        $select->offset($limit * $page);
    	return $this->getCacheableData($select);
    }
    public function countByArtist($artist_id){
    	$artist_id = (int)$artist_id;
        $select = $this->tableGateway->getSql()->select();
        $select->where(array(
        	'artist_id'=>$artist_id
		));       
    	return count($this->getCacheableData($select));
    }
    
}
