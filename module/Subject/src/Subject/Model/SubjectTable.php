<?php
    namespace Subject\Model;
    use DVGroup\Db\Model\BaseTable;
    use Zend\Db\Sql\Sql;
    use DVGroup\Common\CommonLibs;
    use Zend\Db\TableGateway\AbstractTableGateway;
    use Zend\Db\TableGateway\TableGateway;
    use Zend\Db\Sql\Select;
    use Zend\Db\Sql\Expression;
    use Zend\ServiceManager\ServiceLocatorInterface;
    use Zend\ServiceManager\ServiceLocatorAwareInterface;


    class SubjectTable extends BaseTable
    {
        const EXPIRE_CACHE = 7200;
        public $CACHE = '_CACHE';
        public $QUEUE = 'queue_rd:a:';
        public $QUEUE_LAST_INSERT = 'queue_rd:s:id';
        public $SUBJECT = 'Subject';
        public $id;
        public $name;
        public $name_search;
        public $order;
        public $status;
        public $type;
        public $isParent;
        public $parentId;
        public $slug;
        public $totalMusic;
        public $keyword;
        public $keyword_en;
        public $hot;
        public $created_by;
        public $dateCreate;

        public function getSubjectById($id)
        {
            $id     = (int)$id;
            $rowset = $this->tableGateway->select(array('id' => $id));
            $row    = $rowset->current();
            if (!$row) {
                throw new \Exception("Could not find row $id");
            }
            return $row;
        }

        public function getSubjectBySlug($slug)
        {
            $rowset = $this->tableGateway->select(array('slug' => $slug));
            $row    = $rowset->current();
            if (!$row) {
                return null;
            }
            return $row;
        }

        public function fetchAll()
        {
            $resultSet = $this->tableGateway->select();
            $array     = array();
            foreach ($resultSet as $value) {
                array_push($array, $value);
            }
            return $array;
        }

        public function fetchAllByField($field, $action, $value)
        {
            $resultSet = $this->tableGateway->select(
                function (Select $select) use ($field, $value, $action) {
                    $select->where(array($field . $action . $value));
                }
            );
            $array     = array();
            foreach ($resultSet as $value) {
                array_push($array, $value);
            }
            return $array;
        }

        public function getList($offset, $limit, $where = null)
        { //where is array
            $sql    = new Sql($this->tableGateway->adapter);
            $select = $sql->select(array('s' => $this->tableGateway->getTable()));
            //$select->columns(array('id', 'name', 'slug'));
            $select->join(array('m' => 'tb_subject_music'), 's.id=m.id_subject',
                array('total' => new Expression('count(m.id)')),
                'left'
            );
            $select->where($where);
            $select->group('s.id');
            $select->order('s.id DESC')->limit($limit)->offset($offset);
            $statement = $sql->prepareStatementForSqlObject($select);
            $sqs = $statement->getSql();
            $resultSet = $statement->execute();
            $array     = array();
            foreach ($resultSet as $value) {
                array_push($array, $value);
            }

            return $array;
        }

        public function countAll($where = null)
        {
            $resultSet = $this->tableGateway->select(function (Select $select) use ($where) {
                if (!is_null($where))
                    $select->where($where); //where is array
            })->count();
            return $resultSet;
        }
        //insert redis
        public function insertQueue($type, $arrKey)
        {
            $redis   = $this->getRedis();
            $last_id = $redis->get($this->QUEUE_LAST_INSERT);
            $last_id++;
            $redis->set($this->QUEUE_LAST_INSERT, $last_id, $expire = 7200); //save last id
            $args              = array();
            $args['time']      = time();
            $args['propeties'] = json_encode($arrKey);
            $args['id']        = $last_id;
            $key               = $this->QUEUE . $last_id;
            $args['name']      = $this->SUBJECT . ':' . $type;
            $redis->hmset($key, $args);
        }

        public function insertSubjectType($type, $data)
        {
            $redis      = $this->getRedis();
            $keySubject = $this->SUBJECT . ':' . $type;
            if (isset($data) && count($data) > 0) {
                foreach ($data as $val) {
                    $redis->LPUSH($keySubject, $val['id']);
                    $keyDetail = $this->SUBJECT . ':detail:' . $val['id'];
                    $redis->hmset($keyDetail, $val); //insert to detail subject
                }
            }
        }

        public function array_slice_key($array, $offset, $len = -1)
        {
            $return = array();
            if (!is_array($array))
                return FALSE;
            $length = $len >= 0 ? $len : count($array);
            $keys   = array_slice(array_keys($array), $offset, $length);
            if($keys && count($keys)>0){
                foreach ($keys as $key) {
                    $return[$key] = $array[$key];
                }
            }
            return $return;
        }

        public function getData($type,$keySubject,$where,$offset,$perPage){
           $redis = $this->getRedis();
            $arrSubject = array();
            $data = array();
            if (!$redis || !$redis->exists($keySubject)) { //check exist redis
                $list        = $this->SUBJECT . ':' . $type;
                if($redis){
                    $listsubject = $redis->lrange($list, 0, -1);
                    if(count($listsubject)>=$perPage){
                        arsort($listsubject);
                        if ($listsubject && count($listsubject) > 0) {
                            $listsubject = $this->array_slice_key(array_unique($listsubject), $offset, $perPage);
                            if($listsubject && count($listsubject)>0){
                                foreach ($listsubject as $val) {
                                    $data[] = $redis->hgetall($this->SUBJECT . ':detail:' . $val);
                                }
                            }
                        }
                    }
                }
                if (empty($data))
                    $data = $dataSQL = $this->getList($offset, $perPage, $where);
                if ($data && count($data) > 0) {
                    foreach ($data as $k => $val) { //set array
                        $argSubId[]                               = $val['id'];
                        $arrSubject[$k]['subject']                = (array)$val;
                        $day                                      = date('Y-m-d');
                        $subjectKey                               = $this->CACHE . ':SUBJECT:USER-ONLINE:' . $val['slug'] . ':' . $day;
                        $subjectUserlisten  = $this->SUBJECT . ':UserOnline:' . $val['slug'].':*'; //user listen
                        if($redis)
                            $arrSubject[$k]['subject']['totallisten'] = ($redis->exists($subjectKey) ? $redis->exists($subjectKey) : count($redis->keys($subjectUserlisten)));
                        else
                            $arrSubject[$k]['subject']['totallisten'] = ($val['total']>0) ? rand(0,5) : 0;
                    }
                    if($redis)
                        $redis->set($keySubject, CommonLibs::Zip($arrSubject), self::EXPIRE_CACHE); //insert cache

                    if (isset($dataSQL) && count($dataSQL) > 0 && $redis) {
                        $this->insertQueue($type, $argSubId); //insert queue
                        $this->insertSubjectType($type, $data); //insert subject redis
                    }
                }
            } else {
                $subject     = $redis->get($keySubject);
                $arrSubject  = CommonLibs::UnZip($subject);
                foreach($arrSubject as $k=> $val){ //get visit
                    $userlisten_cache = $this->CACHE . ':SUBJECT:USER-ONLINE:'.$val['subject']['slug'] ;
                    $subjectUserlisten  = $this->SUBJECT . ':UserOnline:' . $val['subject']['slug'].':*'; //user listen
                    $arrSubject[$k]['subject']['totallisten'] = ($redis->exists($userlisten_cache)?$redis->get($userlisten_cache):count($redis->keys($subjectUserlisten)));
                }
            }
            return $arrSubject;
        }
    }
