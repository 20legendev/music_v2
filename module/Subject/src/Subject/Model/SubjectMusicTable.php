<?php
    namespace Subject\Model;

    use DVGroup\Db\Model\BaseTable;
    use Zend\Db\TableGateway\TableGateway;
    use Zend\Db\Sql\Select;
    use Zend\Db\Sql\Sql;

    class SubjectMusicTable extends BaseTable
    {

        public $id;
        public $slug_subject;
        public $id_subject;
        public $id_music;
        public $title_music;
        public $slug;
        public $identify;
        public $hitcount;

        /*
         * Author: KienNT
         * Get subject detail for a song
         * input: music_id (from tb_music_title_forread)
         * output: subject information include name
         */
        
        public function getSubjectByMusicId($music_id){
        	$table_name = $this->tableGateway->getTable();
        	$select = $this->tableGateway->getSql()->select();
        	$select->join(array('s'=>'tb_subject'), $table_name.'.id_subject = s.id', array('name', 'name_search'), 'left');
        	$select->where(array(
        		$table_name.'.id_music' => $music_id
        	));
        	$result = $this->getCacheableData($select);
        	if(count($result) > 0) return $result[0];
        	return NULL;
        }
        
        public function getSubjectById($id)
        {
            $id     = (int)$id;
            $rowset = $this->tableGateway->select(array('id' => $id));
            $row    = $rowset->current();
            if (!$row) {
                return false;
            }
            return $row;
        }

        public function fetchAll()
        {
            $resultSet = $this->tableGateway->select();
            $array     = array();
            foreach ($resultSet as $value) {
                array_push($array, $value);
            }
            return $array;
        }

        public function getMusicBySubject($subId, $offset = 0, $limit = 10)
        {
            $sql    = new Sql($this->tableGateway->adapter);
            $select = $sql->select(array('s' => $this->tableGateway->getTable()));
            $select->join(array('m' => 'tb_music_title_foread'), 's.id_music = m.id',
                array('artist_id', 'cate_id', 'file', 'file_source', 'like', 'link', 'nhacsi_id', 'point', 'quality', 'source', 'thumnail', 'thumnail_source', 'title', 'view'),
               // Select::SQL_STAR ,
                'left'
            );
            $select->join(array('cat' => 'tb_cate'), 'm.cate_id = cat.id', array('cat_name' => 'name', 'cat_slug' => 'slug'), 'left');
            $select->join(array('art' => 'tb_artist'), 'm.artist_id = art.id', array('artist_name' => 'name'), 'left');
            $select->where(array('s.id_subject' => $subId));
            $select->group('s.id_music');
            $select->order('s.id DESC')->limit($limit)->offset($offset);
            $statement = $sql->prepareStatementForSqlObject($select);
            $resultSet = $statement->execute();
            $array     = array();
            foreach ($resultSet as $value) {
                array_push($array, $value);
            }

            return $array;
        }

        public function fetchAllByField($field, $action, $value, $limit = 10)
        {
            $resultSet = $this->tableGateway->select(
                function (Select $select) use ($field, $action, $value, $limit) {
                    $select->where(array($field . $action . $value));
                    $select->order('id DESC');
                    $select->limit($limit);
                }
            );

            $array = array();
            foreach ($resultSet as $value) {
                array_push($array, $value);
            }
            return $array;
        }

        public function getList($offset, $limit)
        {
            $resultSet = $this->tableGateway->select(function (Select $select) use ($limit, $offset) {
                $select->order('order DESC')->limit($limit)->offset($offset);
            });
            $array     = array();
            foreach ($resultSet as $value) {
                array_push($array, $value);
            }
            return $array;
        }

        public function countAll()
        {
            $resultSet = $this->tableGateway->select()->count();
            return $resultSet;
        }


        //get count music by subject
        public function getCountMusicBySubjectId($subid)
        {
            $resultSet = $this->tableGateway->select(
                function (Select $select) use ($subid) {
                    $select->where(array('id_subject' => $subid));
                }
            )->count();
            return $resultSet;
        }

    }
