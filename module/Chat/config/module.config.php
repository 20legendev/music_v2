<?php

return array(
     'controllers' => array(
         'invokables' => array(
             'Chat\Controller\Chat' => 'Chat\Controller\ChatController', 
         ),
     ),

     // The following section is new and should be added to your file
     'router' => array(
         'routes' => array(
             'chat' => array(
                 'type'    => 'segment',
                 'options' => array(
                     'route'    => '/chat[/][:action][/:id]',
                     'constraints' => array(
                         'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                         'id'     => '[0-9]+',
                     ),
                     'defaults' => array(
                         'controller' => 'Chat\Controller\Chat',
                         'action'     => 'index',
                     ),
                 ),
             ),
         ),
     ),
 );