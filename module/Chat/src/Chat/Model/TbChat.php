<?php
    /**
    * Author: Phí Đình Thọ
    * Class TbChat
    * Sử lý Redis cho module Chat
    */
    namespace Chat\Model;
    use Chat\Model\RedisActive;

    class TbChat extends RedisActive
    {  
        /**
        *  Khai báo và trả về các thuộc tính của model
        */
        public function attributes()
        { 
            return ['id','user_from','user_to','room_id','content','date'];
        }

        /**
        *  Validate dữ liệu, lưu ý, để đảm bảo model chạy chính xác thì tất cả thuộc tính đều phải validate
        *  Các validate đã viết sẵn : bao gồm hàm (required, unique, integer, float, autoIncrement, datetime,...) và method (min, max, >,>=,<,<=,like, notlike...) , Có thể phát triển thêm bất kì validate nào nếu muốn 
        */
        public function rules()
        {
            return [
                [['user_from','content'], 'required','messages'=>['user_from'=>'user_from cannot be blank','content' => 'content cannot be blank']],
                [['id','room_id'], 'integer','messages'=>['id'=>'id must be an integer','room_id'=>'room_id must be an integer']],
                [['id'], 'autoIncrement'],
                [['date'], 'datetime','messages'=>['date'=>'datetime value is invalid']],
                [['content'], ['max',255],'messages'=>['content'=>'content must have less than 255 chars']],
                [[['id','room_id'],['content']],'unique','messages'=>['id'=>'id , room_id must be unique','room_id'=>'id , room_id must be unique','content' => 'content must be unique']]
            ];
        }                

        /**
        *  Chuyển attribus thành Label để hiển thị Frontend hoặc trong các form...
        */
        public function attributeLabels()
        {
            return [
                'id' => 'ID',
                'user_from'  => 'Người gửi',
                'user_to' => 'Người nhận',
                'room_id' => 'Phòng chat',
                'content' => 'Nội dung',
                'date' => 'Thời gian', 
            ];
        }    
}