<?php
namespace News\Model;
 use Zend\Db\TableGateway\TableGateway;
  use Zend\Db\Sql\Sql;
 use Zend\Db\Sql\Select;
 use Zend\Db\Sql\Where;
Class TbCategoryArticle
{
    protected $TableGateway;
    public function __construct(TableGateway $TableGateway)
    {
        $this->TableGateway=$TableGateway;
    }
    public function test()
    {
        //echo '123123';
    }
    public function getAll_Article_Cate()
    {
        return $this->TableGateway->select();
    }
    public function All_Article_Cate_array()
    {
        $tmp=$this->getAll_Article_Cate();
        $allCate=array();     
        foreach($tmp as $value)
        {
            $allCate[$value->id]=array( 'slug'=>$value->slug,
                                        'category_name'=>$value->category_name,                                           
                                        'parent_id'=>$value->parent_id,
                                        'id'=>$value->id);
        }
        return $allCate;
    }
    public function getCateBySllug($slug)
    {
        $sql=new Sql($this->TableGateway->getAdapter());
        $select=new Select($this->TableGateway->getTable());
        $select->columns(['id','category_name','slug','parent_id']);
        $select->where(['slug'=>$slug]);
        return $this->TableGateway->selectWith($select)->current();
        
    }
}