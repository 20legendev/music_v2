<?php
namespace News\Model;
 use Zend\Db\TableGateway\TableGateway;
 use Zend\Db\Sql\Sql;
 use Zend\Db\Sql\Select;
 use Zend\Db\Sql\Where;
Class TbArticle
{
    protected $TableGateway;
    public function __construct(TableGateway $TableGateway)
    {
        $this->TableGateway=$TableGateway;
    }
    public function getall()
    {
        return $this->TableGateway->select();
    }
    public function tophome($category_id='',$limit=5,$order='is_home')
    {
      //  echo $category_id;
        $sql=new Sql($this->TableGateway->getAdapter());
        $select=new Select($this->TableGateway->getTable());
        $select->columns(['id','identify','title','slug','description','image_thumb','image_path','created_time']);
         $where=new Where();
         $where->AND->equalTo('is_publish',1);
        $where->AND->equalTo('status',1);  
        //$select->where(['is_publish'=>1,'status'=>1]);
        if($category_id)$where->AND->equalTo('category_id',$category_id);
        $select->where($where);
        $select->order($order.' DESC');
        $select->limit($limit);
       // echo $select->getSqlString();
        return $this->TableGateway->selectWith($select);
       // $this->TableGateway->se
    }
    public function getbyid($id)
    {
        $rowset=$this->TableGateway->select(['id'=>$id]);
        $row=$rowset->current();
        if(!$row)return false;
        return $row;
    }
    //lay tin theo the loai
    public function getNewsByCaterory($category_id,$sort='is_home',$order='DESC',$limit=5)
    {
        $sql=new Sql($this->TableGateway->getAdapter());
        $select=new Select($this->TableGateway->getTable());
        $select->columns(['id','identify','title','slug','description','image_thumb','image_path','created_time']);
        $select->where(['is_publish'=>1,'status'=>1,'category_id'=>$category_id]);
        $select->order($sort.' '.$order);
        $select->limit($limit);
        return $this->TableGateway->selectWith($select);
    }
    // lay tin cho nhung the loai con lai
    public function getNews_theloaikhac($sort='id',$order='DESC',$limit=5)
    {
       // if(!empty($arr_id))return false;
        $sql=new Sql($this->TableGateway->getAdapter());
        $select=new Select($this->TableGateway->getTable());
        $select->columns(['id','identify','title','slug','description','image_thumb','image_path','created_time']);
        $where=new Where();
        $where->AND->notEqualTo('category_id',2);
        $where->AND->notEqualTo('category_id',3);
        $where->AND->notEqualTo('category_id',4); 
        $where->AND->equalTo('is_publish',1);
        $where->AND->equalTo('status',1);      
        
        $select->where($where);
        $select->order($sort.' DESC');
        $select->limit($limit);
        return $this->TableGateway->selectWith($select);
    }
    public function getNews_By_Category_slug($slug,$page=1,$limit=15,$sort='id',$order='DESC')
    {
        $sql=new Sql($this->TableGateway->getAdapter());
        $select=new Select($this->TableGateway->getTable());
        $select->columns(['id','identify','title','slug','description','image_thumb','image_path','created_time']);
        $select->join('tb_article_category_new',$this->TableGateway->getTable().'.category_id=tb_article_category_new.id',['category_slug'=>'slug'],'left');
                
        $select->where(['tb_article_category_new.slug'=>$slug,
                        $this->TableGateway->getTable().'.is_publish'=>1,
                        $this->TableGateway->getTable().'.status'=>1,                       
                        ]);
        $select->order($sort.' '.$order);
        $select->limit($limit);
  
        $offset=($page-1)*$limit;
       
        $select->offset($offset);  
       // echo $select->getSqlString();
        return $this->TableGateway->selectWith($select);
    }
    public function getNewsDetailBySlug($slug)
    {
        //$rowset=$this->TableGateway->select();
        $sql=new Sql($this->TableGateway->getAdapter());
        $select=new Select($this->TableGateway->getTable());
        $select->join('tb_article_content_new',$this->TableGateway->getTable().'.id=tb_article_content_new.article_id',['content'=>'content_html'],'left')
                ->join('tb_article_category_new',$this->TableGateway->getTable().'.category_id=tb_article_category_new.id',['category_name'=>'category_name','category_slug'=>'slug'],'left');
        $select->where([$this->TableGateway->getTable().'.slug'=>$slug]);
        return $this->TableGateway->selectWith($select)->current();
        
    }
    public function getArticleByTags($tag_slug)
    {
        $sql=new Sql($this->TableGateway->getAdapter());
        $select=new Select($this->TableGateway->getTable());
        $select->join('tb_article_tag_relation',$this->TableGateway->getTable().'.id=tb_article_tag_relation.article_id',['tag_id'=>'tag_id'],'left')
                ->join('tb_article_tag','tb_article_tag_relation.tag_id=tb_article_tag.id',['tag_identify'=>'identify','tag_slug'=>'slug','tag_name'=>'tag_name'],'left');
        $select->where(['tb_article_tag.slug'=>$tag_slug]);
        return $this->TableGateway->selectWith($select);
    } 


}