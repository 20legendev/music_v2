<?php
namespace News\Widget;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;
use DVGroup\Redis\Redis;
class TinNoiBat extends AbstractHelper{

    protected $serviceLocator;
    protected function getTbArticle()
    {
        return $this->serviceLocator->get('TbArticle');        
    }
    public function __invoke(){
        return $this;
           

    }
    public function tinnoibat($category_id=0)
    {

         $keyrd='_CACHE:HOME_NEWS_WIDGET:'.$category_id;
         $redis=new Redis();
         $array=$redis->_Get($keyrd);
         if(!$array||empty($array))
         {
            $k=$this->getTbArticle()->tophome($category_id);         
            $array=[];
            foreach($k as $value)
            {
                $array[]=$value;
            }
            if(!empty($array))$redis->_Set($keyrd,$array,7200);
         }
            
            return $this->getView()->render('news/widget/news_home/tinnoibat', array('arr' => $array));
    }
    public function tinchuyenmuc($cat,$sort='id')
    {
        $keyrd='_CACHE:HOME_NEWS_WIDGET:'.$cat['id'];
        $redis=new Redis();
        $array=$redis->_Get($keyrd);
        if(!$array||empty($array))
        {
            $k=$this->getTbArticle()->getNewsByCaterory($cat['id'],$sort);
            $array=[];
            foreach($k as $value)
            {
                $array[]=$value;
            }
            if(!empty($array))$redis->_Set($keyrd,$array,7200);
        }
        $array['category']=$cat;
        return $this->getView()->render('news/widget/news_home/chuyenmuc', array('arr' => $array));
//        if($id==2)return $this->getView()->render('news/widget/news_home/tinnhacviet', array('arr' => $array));
//        if($id==3)return $this->getView()->render('news/widget/news_home/nhacchaua', array('arr' => $array));
//        if($id==4)return $this->getView()->render('news/widget/news_home/nhacaumy', array('arr' => $array));
//        if($id==1)return $this->getView()->render('news/widget/news_home/theloaikhac', array('arr' => $array));
    }
    //public function tinnhacviet()
//    {
//        $k=$this->getTbArticle()->getNewsByCaterory(2);
//        $array=[];
//            foreach($k as $value)
//            {
//                $array[]=$value;
//            }
//            
//            return $this->getView()->render('news/widget/news_home/tinnhacviet', array('arr' => $array));
//    }
//    public function nhacchaua()
//    {
//        $k=$this->getTbArticle()->getNewsByCaterory(3);
//        $array=[];
//            foreach($k as $value)
//            {
//                $array[]=$value;
//            }
//            
//            return $this->getView()->render('news/widget/news_home/nhacchaua', array('arr' => $array));
//    }
//    public function nhacaumy()
//    {
//        $k=$this->getTbArticle()->getNewsByCaterory(4);
//        $array=[];
//            foreach($k as $value)
//            {
//                $array[]=$value;
//            }
//            
//            return $this->getView()->render('news/widget/news_home/nhacaumy', array('arr' => $array));
//    }
//    public function theloaikhac()
//    {
//        $k=$this->getTbArticle()->getNews_theloaikhac();
//        $array=[];
//            foreach($k as $value)
//            {
//                $array[]=$value;
//            }
//            
//            return $this->getView()->render('news/widget/news_home/theloaikhac', array('arr' => $array));
//    }
    public function setServiceLocator(ServiceManager $serviceLocator){
        $this->serviceLocator = $serviceLocator;
    }
}?>