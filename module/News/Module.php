<?php
namespace News;

 use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
 use Zend\ModuleManager\Feature\ConfigProviderInterface;
 use Zend\Db\TableGateway\TableGateway;
 use News\Model\TbCategoryArticle;
 use News\Model\ArticleTag;
 use News\Model\TbArticle;
  use Zend\Db\ResultSet\ResultSet;

 class Module implements AutoloaderProviderInterface, ConfigProviderInterface
 {
     public function getAutoloaderConfig()
     {
         return ['Zend\Loader\StandardAutoloader' =>
                        ['namespaces' =>
                                    [__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,],], ];
     }

     public function getConfig()
     {
         return include __DIR__ . '/config/module.config.php';
     }
     public function getServiceConfig()
     {
        return 
        [   
            'factories'=>
            [
                'TbCategoryArticle' => function($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $tableGateway = new TableGateway('tb_article_category', $dbAdapter, null, $resultSetPrototype);
                        $table = new TbCategoryArticle($tableGateway);
                        return $table;
                 }, 
                 'TbArticle' => function($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $tableGateway = new TableGateway('tb_article_info', $dbAdapter, null, $resultSetPrototype);
                        $table = new TbArticle($tableGateway);
                        return $table;
                 }, 
                 'TbTagsArticle' => function($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $tableGateway = new TableGateway('tb_article_tag_relation', $dbAdapter, null, $resultSetPrototype);
                        $table = new \News\Model\TbTagsArticle($tableGateway);
                        return $table;
                 }, 
                 'TbArticleRd'=>function($sm)
                {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return new \News\Model\TbArticleRd(new \Zend\Db\TableGateway\TableGateway('tb_article_info', $dbAdapter));
                },
                 
                 'Crawle' => function($sm) {
                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                        $resultSetPrototype = new ResultSet();
                        $tableGateway = new TableGateway('tb_article_tag', $dbAdapter, null, $resultSetPrototype);
                        $table = new \News\Model\Crawle($tableGateway);
                        return $table;
                 }, 
                 'ArticleTag' => function ($sm) {
                 	$dbAdapter = $sm->get ( 'Zend\Db\Adapter\Adapter' );
                 	return new ArticleTag ( new TableGateway ( 'tb_article_tag', $dbAdapter ) );
                 }
            ]
        ];
     }
public function getViewHelperConfig()
    {
        return [
            'factories' => [
                 'tinnoibat' => function ($sm) {
                $serviceLocator = $sm->getServiceLocator();
                $viewHelper = new \News\Widget\TinNoiBat();
                $viewHelper->setServiceLocator($serviceLocator);
                return $viewHelper;
                },
            ]
        ];  
    }

 }
