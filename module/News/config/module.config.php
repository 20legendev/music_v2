<?php
return ['controllers'     =>[
            'invokables'     =>[   
                    'News\Controller\Index'         => 'News\Controller\IndexController',
                    'News\Controller\Category'         => 'News\Controller\CategoryController',
                    'News\Controller\Tags'         => 'News\Controller\TagsController',
                    ]],
    'router'          =>[
            'routes'         => [
// Route cho Module News==========================================================================================           
            //dang ky-----------------------------
             'Article_Category'    => ['type'    => 'segment',
                             'options' => [ 'route'       => '/tin-tuc/[:slug][/page/:page].html', 
                                            'constraints'   =>['slug'   => '[a-zA-Z0-9_-]*',
                                                               'page'   =>'[0-9]+',  ],                                         
                                            'defaults'    => ['controller' => 'News\Controller\Index',
                                                            'action'     => 'category']]],
             'Article_All'    => ['type'    => 'segment',
                             'options' => [ 'route'       => '/tin-tuc.html',      
                                            'defaults'    => ['controller' => 'News\Controller\Index',
                                                            'action'     => 'index']]],
             'Article_Detail'    => ['type'    => 'segment',
                             'options' => [ 'route'       => '/tin-tuc[/:slug].[:identify].html',  
                                            'constraints'   =>['slug'   => '[a-zA-Z0-9_-]*',
                                                               'identify'   =>'[a-zA-Z0-9_-]*',  ],                                       
                                            'defaults'    => ['controller' => 'News\Controller\Index',
                                                            'action'     => 'detail']]],
             'Article_Tags'    => ['type'    => 'segment',
                             'options' => [ 'route'       => '/news[/tags/:slug].html',  
                                            'constraints'   =>['slug'   => '[a-zA-Z0-9_-]*',
                                                               'identify'   =>'[a-zA-Z0-9_-]*',  ],                                       
                                            'defaults'    => ['controller' => 'News\Controller\Tags',
                                                            'action'     => 'index']]],



    //===User Module===================================================================================================
    
   
    
            ], //routes
        ], //router
//end route++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     'view_manager' => [
         'template_path_stack' =>[
             'news' => __DIR__ . '/../view',
         ],
     ],
];
 ?>