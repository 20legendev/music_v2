<?php
    return [
    'controllers'     =>[
            'invokables'     =>[   
                    //Home Module
                    'Home\Controller\Index'         => 'Home\Controller\IndexController',
                    ]],
    'router'          =>[
            'routes'         => [
                    'home'          => [
                            'type'          => 'Literal',
                            'options'       =>[
                                    'route'          => '/',
                                    'defaults'       =>[
                                            '__NAMESPACE__' => 'Home\Controller',
                                            'controller'    => 'Index',
                                            'action'        => 'index',], //defaults
                                            ], //options
                            'may_terminate' => true,
                            'child_routes'  =>[
                                    'default'         =>[
                                            'type'          => 'Segment',
                                            'options'       =>[
                                                'route'       => '/[:controller[/:action[/]]]',
                                                'constraints' =>[
                                                        'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                        'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                                                        ], //constraints
                                                'defaults'    =>[], //defaults
                                           ], //options
                                ], //default
                            ], //child_routes
                ], //users
    
            ], //routes
        ], //router
//end route++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'fr_CA',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
                'text_domain' => __NAMESPACE__,
            ),
        ),
    ),



        'view_manager'    => array(
            'display_not_found_reason' => true,
            'display_exceptions'       => true,
            'doctype'                  => 'HTML5',
            'not_found_template'       => 'error/404',
            'exception_template'       => 'error/index',
            'template_map'             => array(
                'layout/home'    =>   LAYOUT_DEFAULT.'.phtml',
                'error/404'      => __DIR__ . '/../view/error/404.phtml',
                'error/index'    => __DIR__ . '/../view/error/index.phtml',
                'layout/layout'    => LAYOUT_DEFAULT.'.phtml',//layout mac dinh
            ),
            'template_path_stack'      => array(
                'home' => __DIR__ . '/../view',
            ),
        ),
        'module_layouts'  => array( //set layout module
            'Home'    => 'layout/home',
            'Subject' => 'layout/home',
            'Artist' => 'layout/home',
        ),
    ];
