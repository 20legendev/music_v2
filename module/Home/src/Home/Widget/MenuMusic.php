<?php
namespace Home\Widget;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;
use DVGroup\Redis\Redis;

class MenuMusic extends AbstractHelper{

    protected $serviceLocator;
    protected function getCateTable()
    { 
        return $this->serviceLocator->get('Cate\Model\TbCate');
    }
    public function __invoke()
    {
        //$view=new ViewModel();
        $keyrd_music_cate='tb_cate:MUSIC:ALLCATE';
        $redis=new Redis();
        $music_cate_arr=$redis->_Get($keyrd_music_cate);
        if(!$music_cate_arr||empty($music_cate_arr))
        {
            $music_cate_arr=$this->getCateTable()->globAllCate(0);
            $redis->_Set($keyrd_music_cate,$music_cate_arr,7200);
           // echo 'db';
        }       
       // $view->music_cate=$music_cate_arr;
        //echo 'rdm';
        $keyrd_video_cate='tb_cate:VIDEO:ALLCATE';
        $video_cate_arr=$redis->_Get($keyrd_video_cate);
        if(!$video_cate_arr||empty($video_cate_arr))
        {
            $video_cate_arr=$this->getCateTable()->globAllCate(1);
            $redis->_Set($keyrd_video_cate,$video_cate_arr,7200);
           // echo 'db';
        }
        //echo 'rdv';        
      //  $view->video_cate=$video_cate_arr;
        //return $view; 
            
            
            
            
            
        return $this->getView()->render('home/widget/MenuMusic',['music_cate' => $music_cate_arr,
                                                                 'video_cate' => $video_cate_arr]);

    }
    public function setServiceLocator(ServiceManager $serviceLocator){
        $this->serviceLocator = $serviceLocator;
    }
}?>