<?php
namespace Home\Widget;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

class BlogMenuWidget extends AbstractHelper{

    protected $serviceLocator;
    protected function getRedis()
    {
        return $this->serviceLocator->get('redis');
    }
    protected function getTbCategoryArticle()
    {
        return $this->serviceLocator->get('TbCategoryArticle');        
    }
    public function __invoke(){
            $k=$this->getTbCategoryArticle()->All_Article_Cate_array();
            return $this->getView()->render('home/widget/BlogMenuWidget', array('arr' => $k));

    }
    public function setServiceLocator(ServiceManager $serviceLocator){
        $this->serviceLocator = $serviceLocator;
    }
}?>