<?php
namespace Home\Widget;

use DVGroup\Auth\AuthUser;
use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

class GlobWidget extends AbstractHelper{

    protected $serviceLocator;
    protected function getTbArticle()
    {
        return $this->serviceLocator->get('TbArticle');        
    }
    public function __invoke(){
        return $this;

    }

    public function getmenu()
    {
        $auth = new AuthUser();
        return $this->getView()->render(LAYOUT_FOLDER . 'menu', [
          'isAuth'   => $auth->isAuthen(),
          'zf_user' => $auth->getUser()
        ]);
    }
    public function getfooter()
    {
         return $this->getView()->render(LAYOUT_FOLDER.'footer');
    }
    public function setServiceLocator(ServiceManager $serviceLocator){
        $this->serviceLocator = $serviceLocator;
    }
}?>