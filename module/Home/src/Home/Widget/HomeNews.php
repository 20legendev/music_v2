<?php
namespace Home\Widget;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;

class HomeNews extends AbstractHelper{

    protected $serviceLocator;
    protected function getTbArticle()
    {
        return $this->serviceLocator->get('TbArticle');        
    }
    public function __invoke(){
            $k=$this->getTbArticle()->tophome();
            $array=[];
            foreach($k as $value)
            {
                $array[]=$value;
            }
            
            return $this->getView()->render('home/widget/HomeNews', array('arr' => $array));

    }
    public function setServiceLocator(ServiceManager $serviceLocator){
        $this->serviceLocator = $serviceLocator;
    }
}?>