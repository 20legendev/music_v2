<?php

    namespace Home\Controller;

    use Zend\Mvc\Controller\AbstractActionController;
    use Zend\View\Model\ViewModel;

    class IndexController extends AbstractActionController
    {
        const PAGE     = 1;
        const PER_PAGE = 15; //số bản ghi trên 1 trang
        const PAGE_RANGE = 5; //số phân trang

        public function indexAction()
        {

            $view       = new ViewModel();
            $subjectHot  = $this->forward()->dispatch('Subject\Controller\Widget', array('action' => 'subject-process', 'page' => self::PAGE, 'perPage' => self::PER_PAGE, 'pageRange' => self::PAGE_RANGE, 'type' => 'hot','template' => 'subject-hot'));
            $view->addChild($subjectHot, 'subjectHot');
            $this->layout('layout/home');
            return $view;
        }

    }
