<?php
namespace User\Form;

use Zend\Form\Element;
use Zend\Form\Element\Text;
use Zend\Form\Element\Password;
use Zend\Form\Element\Email;
use Zend\Form\Element\Date;
use Zend\Form\Element\Number;
use Zend\Form\Form;
use Zend\Form\Element\Csrf;

class UserForm extends Form
{   

    public function __construct($name)
    {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->addlelement();
    }
    public function addlelement()
    {
        $name=new Text('fullname');
        $name->setLabel('Tên Đầy Đủ:*')
             ->setAttribute('class','form-control')
             ->setAttribute('required',true);
        $this->add($name);
        
        $username=new Text('username');
        $username->setLabel('Tên Đăng Nhập:*')
                 ->setAttribute('class','form-control')
                 ->setAttribute('required',true);
        $this->add($username);
        
        $password=new Password('password');
        $password->setLabel('Mật Khẩu:*')
                 ->setAttribute('class','form-control')
                 ->setAttribute('required',true);
        $this->add($password);
        
        $repassword=new Password('repassword');
        $repassword->setLabel('Nhập Lại Mật Khẩu:* ')
                 ->setAttribute('class','form-control')
                 ->setAttribute('required',true);
        $this->add($repassword);
        
        $email=new Email('email');
        $email->setLabel('Email:*')
              ->setAttribute('class','form-control')
              ->setAttribute('required',true);
        $this->add($email);
        
        $birthday=new Text('birthday');
        $birthday->setLabel('Ngày Sinh:')
                 ->setAttribute('class','form-control')
                 ->setAttribute('required',false);
        $this->add($birthday);
        
        $adress=new Text('adress');
        $adress->setLabel('Địa Chỉ:')
               ->setAttribute('class','form-control');
        $this->add($adress);

        $sdt=new Text('sodienthoai');
        $sdt->setLabel('Số Điện Thoại:')
            ->setAttribute('class','form-control');
            $this->add($sdt);
        $security=new Csrf('security');
        $this->add($security);
  
        
    }

}