<?php

namespace Artist\Source;

class ArtistCommon {
	protected static $server = 'http://video.kenhtin.net';
	
	/*
	 * author: KienNT
	 * get profile picture, banner for artist
	 * input: resource = filename, $name_search = artist_slug, $folder = avatar/banner
	 */
	public static function getUrlImageArtist($resource = '', $name_search = '', $folder = 'avatar', $cate_id = 0) {
		if ($cate_id && $cate_id > 0) {
			$imageUrl = ArtistCommon::$server . '/music_file/images/' . $folder . '/' . $cate_id . '/' . str_replace ( ' ', '-', $name_search ) . '/' . $resource;
		} else {
			$imageUrl = ArtistCommon::$server . '/music_file/images/' . $folder . '/' . str_replace ( ' ', '-', $name_search ) . '/' . $resource;
		}
		return $imageUrl;
	}
	
	/*
	 * author: KienNT
	 * get album real image path
	 * input: resource = filename, $namesearch = album_slug
	 */
	public static function getAlbumImage($resource = '', $name_search = '') {
		return ArtistCommon::$server . '/music_file/images/album/' . str_replace ( ' ', '-', $name_search ) . '/' . $resource;
	}
	
	/*
	 * author: KienNT
	 * get album real image path
	 * input: resource = filename, $namesearch = album_slug
	 */
	public static function getRealMediaPath($resource, $cate_key, $type = 0, $artist_id, $identify, $remoteIp = NULL, $expires = 0) {
		$folder = "audio";
		$key = '';
		if ($type == 1) {
			$folder = "video";
			$key = 'video-';
			$file = '/music_file/files/' . $folder . '/' . str_replace ( "video-", "", $cate_key ) . '/' . $artist_id . '/' . $identify . '/' . $resource;
			$ip = '42.112.210.35';
			$file = CommonLibs::generateUrl ( $file, $ip );
		} else {
			$file = ArtistCommon::$server . '/music_file/files/' . $folder . '/' . str_replace ( "video-", "", $cate_key ) . '/' . $artist_id . '/' . $identify . '/' . $resource;
		}
		return $file;
	}
}
