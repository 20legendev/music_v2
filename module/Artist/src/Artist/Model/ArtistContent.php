<?php
namespace Artist\Model;

use DVGroup\Db\Model\BaseTable;

class ArtistContent extends BaseTable {
    
	public function getByArtistId($artist_id){
		$select = $this->tableGateway->getSql()->select();
		$select->where(array('artist_id'=>$artist_id));
		$result = $this->getCacheableData($select);
		return count($result) > 0 ? $result[0] : NULL;
	}
	
    public function getByNameSearch($name){
    	$select = $this->tableGateway->getSql()->select();
    	$select->where(array(
			'name_search'=>$name
		));
		return $this->getCacheableData($select);
    }
	
	public function getById($artist_id){
		
	}
    
}