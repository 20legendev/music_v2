<?php
namespace Cate;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Cate\Model\TbCate;
use Cate\Model\Cate;

use Cate\Model\ArtistCountry;
use Zend\Session\Container;
use Cate\Model\MusicContentForead;
use Zend\Mvc\ModuleRouteListener;

use Cate\Form\ArtistFilterForm;
use Zend\Mvc\MvcEvent;
use User\Model\Validator;

class Module {
	public function onBootstrap(MvcEvent $e) {
		$eventManager = $e -> getApplication() -> getEventManager();
		$moduleRouteListener = new ModuleRouteListener();
		$moduleRouteListener -> attach($eventManager);
		$eventManager -> attach(MvcEvent::EVENT_DISPATCH, array($this, 'preload'), 100);
	}


	//@tienhvt ham nay load truoc khi thuc thi cac action trong controller,
	//cac action can phai login
	//set ngon ngu mac dinh

	function preload(MvcEvent $event) {

		$list_route_need_login = array('User\Controller\Index-profile', 'User\Controller\Index-logout', );
		$controller = $event -> getRouteMatch() -> getParam('controller');
		$action = $event -> getRouteMatch() -> getParam('action');
		$request_route = $controller . '-' . $action;
		$validator = new Validator();
		// if($validator->isValid()){echo "xin chao ".$validator->getUser();}
		if (in_array($request_route, $list_route_need_login)) {
			if (!$validator -> isValid())
				die('ban can phai dang nhap');

		}
		// thay doi ngon ngu. Mac dinh la en_US, bien $userlang dc lay tu thong tin user
		//ngon ngu duoc luu trong thu muc module/Application/language
		//phan nao can translate thi o view dung $this->translate('tu can dich',__NAMESPACE__)
		//set langguege
		$userlang = 'vi_VN';
		$lang = isset($userlang) ? $userlang : 'en_US';
		$translator = $event -> getApplication() -> getServiceManager() -> get('translator');

		$translator -> setLocale($lang) -> setFallbackLocale('en_US');
	}

	public function getAutoloaderConfig() {
		return array(
		//             'Zend\Loader\ClassMapAutoloader' => array(
		//                 __DIR__ . '/autoload_classmap.php',
		//             ),
		'Zend\Loader\StandardAutoloader' => array('namespaces' => array(__NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__, ), ), );
	}

	public function getConfig() {
		return
		include __DIR__ . '/config/module.config.php';
	}

	public function getServiceConfig() {
		return array('factories' => array('Cate\Model\TbCate' => function($sm) {
			$tableGateway = $sm -> get('CateTableGateway');
			$table = new TbCate($tableGateway);
			return $table;
		}, 'CateTableGateway' => function($sm) {
			$dbAdapter = $sm -> get('Zend\Db\Adapter\Adapter');
			$resultSetPrototype = new ResultSet();
			$resultSetPrototype -> setArrayObjectPrototype(new Cate());
			return new TableGateway('tb_cate', $dbAdapter, null, $resultSetPrototype);
		}, 'Cate\Model\TbMusicTitleForead' => function($sm) {
			$tableGateway = $sm -> get('TbMusicTitleForeadGateway');
			$table = new \Cate\Model\TbMusicTitleForead($tableGateway);
			return $table;
		}, 'TbMusicTitleForeadGateway' => function($sm) {
			$dbAdapter = $sm -> get('Zend\Db\Adapter\Adapter');
			$resultSetPrototype = new ResultSet();
			// $resultSetPrototype->setArrayObjectPrototype(new Cate());
			return new TableGateway('tb_music_title_foread', $dbAdapter, null, $resultSetPrototype);
		}, 'TbMusicRateForeadGateway' => function($sm) {
			$dbAdapter = $sm -> get('Zend\Db\Adapter\Adapter');
			$resultSetPrototype = new ResultSet();
			// $resultSetPrototype->setArrayObjectPrototype(new Cate());
			return new TableGateway('tb_music_rate', $dbAdapter, null, $resultSetPrototype);
		}, 'Adapter' => function($sm) {
			$dbAdapter = $sm -> get('Zend\Db\Adapter\Adapter');
			return $dbAdapter;
		},
		/* start kiennt */
		'Cate\Model\ArtistCountry' => function($sm) {
			$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');		
			return $table = new ArtistCountry(new TableGateway('tb_artist_country', $dbAdapter));
		}, 'Cate\Form\ArtistFilterForm' => function($sm) {
			$form = new ArtistFilterForm('artist_filter', $sm);
			return $form;
		},
		'MusicContentForead' => function($sm) {
			$dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');		
			return $table = new MusicContentForead(new TableGateway('tb_music_title_content_foread', $dbAdapter));
		}
		));
	}

}
