<?php
return array(
  'controllers'     =>[
            'invokables'     =>[   
                    'Cate\Controller\Index'         => 'Cate\Controller\IndexController',
                    'Cate\Controller\Categories'    => 'Cate\Controller\CategoriesController',
                    'Cate\Controller\Widget'        => 'Cate\Controller\WidgetController',
                    'Cate\Controller\Music'         =>'Cate\Controller\MusicController',

                    ]],
    'router'          =>[
            'routes'         => [    
    //===Cate Module===================================================================================================
            'All_cat' => ['type'    => 'segment',
                             'options' => [ 'route'         => '/cate.html',                                                                                    
                                            'defaults'      => ['controller' => 'Cate\Controller\Index',
                                                                'action'     => 'index']]],
        //catelogy bai hat-------------
            'ChuyenMuc' => ['type'    => 'segment',
                             'options' => [ 'route'         => '/chuyen-muc[/:slug][/:sort].html', 
                                            'constraints'   =>['slug'   => '[a-zA-Z0-9_-]*',
                                                               'sort'   =>'[a-zA-Z][a-zA-Z0-9_-]*'],                                         
                                            'defaults'      => ['controller' => 'Cate\Controller\Categories',
                                                                'action'     => 'show']]],
        //catelogy video-------------
            'Videos' => ['type'    => 'segment',
                             'options' => [ 'route'         => '/videos[/:slug][/:sort].html', 
                                            'constraints'   =>['slug'   => '[a-zA-Z0-9_-]*',
                                                               'sort'   =>'[a-zA-Z][a-zA-Z0-9_-]*'],                                         
                                            'defaults'      => ['controller' => 'Cate\Controller\Categories',
                                                                'action'     => 'video']]], 
        //xem bai hat------------------
            'Music' => ['type'    => 'segment',
                             'options' => [ 'route'         => '/bai-hat/[:slug].[:identify].html', 
                                            'constraints'   =>['slug'   => '[a-zA-Z0-9_-]*',
                                                               'identify'   =>'[a-zA-Z0-9_-]*'],                                         
                                            'defaults'      => ['controller' => 'Cate\Controller\Music',
                                                                'action'     => 'music']]], 
        //xem video------------------------
            'Video' => ['type'    => 'segment',	
                             'options' => [ 'route'         => '/video/[:slug].[:identify].html', 
                                            'constraints'   =>['slug'   => '[a-zA-Z0-9_-]*',
                                                               'identify'   =>'[a-zA-Z0-9_-]*'],                                         
                                            'defaults'      => ['controller' => 'Cate\Controller\Music',
                                                                'action'     => 'video']]], 

            'Artist' => ['type'    => 'segment',
                             'options' => [ 'route'         => '/the-loai-nghe-si[/:category_slug][.:country_slug][/:page].html', 
                                            'constraints'   =>['country_slug'   => '[a-zA-Z0-9_-]*',
                                                               'category_slug'   =>'[a-zA-Z][a-zA-Z0-9_-]*',
                                                               'page'     => '[0-9]*'
                                                               ],   
                                            'defaults'      => ['controller' => 'Cate\Controller\Categories',
                                                                'action'     => 'artist']]],
    //Subject Module====================================================
    
            ], //routes
        ], //router
//end route++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     'view_manager' => [
         'template_path_stack' => [
             'cate' => __DIR__ . '/../view',
         ],
     ],
 );
 ?>