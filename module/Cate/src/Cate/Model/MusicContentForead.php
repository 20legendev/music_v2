<?php
namespace Cate\Model;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

use DVGroup\Db\Model\BaseTable;

class MusicContentForead extends BaseTable {

	/**
	 * @author KienNT
	 * @param int $music_id
	 * @return Ambigous <NULL, unknown>
	 */
	public function getByMusicId($music_id) {
		$select = $this->tableGateway->getSql()->select();
		$select->where(array(
			'id_music' => $music_id
		));
		$result = $this->getCacheableData($select);
		return count($result) > 0 ? $result[0] : NULL;
	}

}
