<?php
namespace Cate\Model;
Class MusicTitleForead{
    public $id;
    public $title;
    public $thumbnail;
    public $thumbnail_source;
    public $createDate;
    public $hitcount;
    public $type;
    public $view;
    public $like;
    public $artist_id;
    public $slug;
    public $cate_id;
    public $identify;
    public $identify_folder;
    public $name_search;
    public function exchangeArray($data)
    {
        $this->id       = (isset($data['id']))  ?   $data['id'] :   0;
        $this->name     =  (isset($data['name']))   ?   $data['name']   :   null;
        $this->name_search      =(isset($data['name_search']))  ?   $data['name_search'] :  null;
        $this->dateCreate        =   (isset($data['dateCreate']))   ?   $data['dateCreate'] :   null;
        $this->createDate    =   (isset($data['createDate']))    ?    $data['createDate']  :   null;
        $this->hitcount   =   (isset($data['hitcount']))    ?   $data['hitcount'] :   null;
        $this->type   =   (isset($data['type']))    ?   $data['type'] :   null;
        $this->view   =   (isset($data['view']))    ?   $data['view'] :   null;
        $this->artist_id   =   (isset($data['artist_id']))    ?   $data['artist_id'] :   null;
        $this->slug   =   (isset($data['slug']))    ?   $data['slug'] :   null;
        $this->cate_id   =   (isset($data['cate_id']))    ?   $data['cate_id'] :   null;
        $this->identify_folder   =   (isset($data['identify_folder']))    ?   $data['identify_folder'] :   null;
    } 
}   
