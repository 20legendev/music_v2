<?php
namespace Cate\Model;
Class Cate{
    public $id;
    public $name;
    public $name_search;
    public $dateCreate;
    public $order;
    public $status;
    public $type;
    public $isParent;
    public $parentId;
    public $slug;
    public $isPlaylist;
    public $totalMusic;
    public function exchangeArray($data)
    {
        $this->id       = (isset($data['id']))  ?   $data['id'] :   0;
        $this->name     =  (isset($data['name']))   ?   $data['name']   :   null;
        $this->name_search      =(isset($data['name_search']))  ?   $data['name_search'] :  null;
        $this->dateCreate        =   (isset($data['dateCreate']))   ?   $data['dateCreate'] :   null;
        $this->order    =   (isset($data['order']))    ?    $data['order']  :   null;
        $this->status   =   (isset($data['status']))    ?   $data['status'] :   null;
        $this->type   =   (isset($data['type']))    ?   $data['type'] :   null;
        $this->isParent   =   (isset($data['isParent']))    ?   $data['isParent'] :   null;
        $this->ParentId   =   (isset($data['ParentId']))    ?   $data['ParentId'] :   null;
        $this->slug   =   (isset($data['slug']))    ?   $data['slug'] :   null;
        $this->isPlaylist   =   (isset($data['isPlaylist']))    ?   $data['isPlaylist'] :   null;
        $this->totalMusic   =   (isset($data['totalMusic']))    ?   $data['totalMusic'] :   null;
    }
}