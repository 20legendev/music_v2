<?php
namespace Cate\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Cate\Model\Cate;
use Cate\Model\TbCate;
use Cate\Model\TbMusicTitleForead;
use Zend\Paginator\Paginator;
 class IndexController extends AbstractActionController
 {
    protected $CateTable;
    protected function getCateTable()
    {
        if(!$this->CateTable)
        {
            $this->CateTable = $this->getServiceLocator()->get('Cate\Model\TbCate');
        }
        return $this->CateTable;
    }
    public function indexAction()
    {
      // var_dump($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
        $view=new ViewModel();
       $menu=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'menu')); 
      // $view->addChild($list,'list');
       $view->addChild($menu,'menu');
       $usertop=$this->forward()->dispatch('Cate\Controller\Widget', array('action' => 'usertop')); 
      // $view->addChild($list,'list');
       $view->addChild($usertop,'user_top');
        return $view; 
    }
    public function getallAction()
    {
        $result=$this->getCateTable()->getAllCate();
        foreach($result as $value)
        {
            var_dump($value);
            echo "---------------------------</br>";
        }
    }
    public function getCateActiveAction()
    {
        $result=$this->getCateTable()->getCateActive();
        foreach($result as $value)
        {
            var_dump($value);
            echo "---------------------------</br>";
        }
        
    }
    public function getslugAction()
    {
        $slug='video-viet-nam';
        $row=$this->getCateTable()->getCateWithSlug($slug);
        var_dump($row);
    }
    public function testAction()
    {
        $tbms=new TbMusicTitleForead($this->getAdapter());
        var_dump($this->getAdapter());
        $kk=$tbms->check();
        
        var_dump($kk->current());
       
    }
    public function test2Action()
    {
        $view=new ViewModel();        
       $tbms=new TbMusicTitleForead($this->getAdapter());
       $kk=$tbms->getListMusicWithCateId(1,0,'id');
       $iteratorAdapter = new \Zend\Paginator\Adapter\Iterator($kk);
       $paginator=new Paginator($iteratorAdapter);
      // $page=$this->params()
 
       
       $paginator->setCurrentPageNumber(1);
       $paginator->setItemCountPerPage(2);
       $paginator->setPageRange(7);
        
        $view->paginator=$paginator;
        //$view->pa2=$paginator;
        return $view;
    }
    private function getAdapter()
    {
        $sm=$this->getServiceLocator();
        return $sm->get('Adapter');
    }


 }
?>