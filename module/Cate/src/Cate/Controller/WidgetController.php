<?php
//
namespace Cate\Widget;
namespace Cate\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Cate\Model\Cate;
use Cate\Model\TbCate;
use Cate\Model\ArtistCountry;
use Cate\Form\ArtistFilterForm;
use DVGroup\Redis\Redis;
use User\Model\Validator;
use Utf8;
use Cate\Model\CateTable;
use Cate\Model\TbMusicTitleForead;
use Zend\Paginator\Paginator;
// this controller group some share functions between controllers
use DVGroup\Operation\BaseController;

class WidgetController extends BaseController
{
    protected $CateTable;
    protected function getCateTable()
    {
        if(!$this->CateTable)
        {
            $this->CateTable = $this->getServiceLocator()->get('Cate\Model\TbCate');
        }

        return $this->CateTable;
    }
    //lam viec voi model TbUser
    private function getTbUser()
    {
        return $this->getServiceLocator()->get('TbUser');
    }
    private function getTbMusicTitleForead()
    {
        return $this->getServiceLocator()->get('Cate\Model\TbMusicTitleForead');
    }
    protected function getRedisLib()
    {
        return $this->getServiceLocator()->get('RedisLib');
    }
    protected function getTbArtist()
    {
        return $this->getServiceLocator()->get('Artist\Model\TbArtist');
    }
    protected function getTbPlaylist()
    {
        return $this->getServiceLocator()->get('Album\Model\TbPlaylist');
    }
    //widget menu
        //menu redis
    public function menuAction()
    {
        $view=new ViewModel();
        $keyrd_music_cate='tb_cate:MUSIC:ALLCATE';
        $redis=new Redis();
        $music_cate_arr=$redis->_Get($keyrd_music_cate);
        if(!$music_cate_arr||empty($music_cate_arr))
        {
            $music_cate_arr=$this->getCateTable()->globAllCate(0);
            $redis->_Set($keyrd_music_cate,$music_cate_arr,7200);
           // echo 'db';
        }       
        $view->music_cate=$music_cate_arr;
        //echo 'rdm';
        $keyrd_video_cate='tb_cate:VIDEO:ALLCATE';
        $video_cate_arr=$redis->_Get($keyrd_video_cate);
        if(!$video_cate_arr||empty($video_cate_arr))
        {
            $video_cate_arr=$this->getCateTable()->globAllCate(1);
            $redis->_Set($keyrd_video_cate,$video_cate_arr,7200);
           // echo 'db';
        }
        //echo 'rdv';        
        $view->video_cate=$video_cate_arr;
        return $view; 
    }
        //menu-no redis
    public function menu_noAction()
    {
        $view=new ViewModel(); 
        $view->music_cate=$this->getCateTable()->globAllCate(0);       
        $view->video_cate=$this->getCateTable()->globAllCate(1);
        return $view; 
    }
    //widget lay list danh muc
        //no -redis
    public function allcateAction()
    {
        $view=new ViewModel();
        $type= $this->params('type');
        $route=$this->params('route');
        $params=$this->params('params');
        if(!isset($type))$type=-1;
        $view->allcate=$this->getCateTable()->globAllCate($type);
        $view->route=$route;
        $view->params=$params;
        return $view;
    }
        //list danh muc-redis
    
    public function allcate_rdAction()
    {
        $view=new ViewModel();
        $type= $this->params('type');
        $route=$this->params('route');
        $params=$this->params('params');
        //type=1 cate video
        //type=0 cate music
        //type=-1 all cate  video,music
        if(!isset($type))$type=-1;
        switch($type){
            case 0:
                $keyrd='tb_cate:MUSIC:ALLCATE';
                break;
            case 1:
                $keyrd='tb_cate:VIDEO:ALLCATE';
                break;
            case -1:
                $keyrd='tb_cate:ALLCATE';
                break;
            
        }
        $redis=new Redis();
        $cate=$redis->_Get($keyrd);
        if(!$cate||empty($cate))
        {
            $tmp=$this->getCateTable()->globAllCate($type);
            $cate=$redis->_Set($keyrd,$tmp,14400);
            echo 'dball';
        }
            
        $view->allcate=$cate;
        $view->route=$route;
        $view->params=$params;
        return $view;
    }
    public function musicrelatedAction()
    {
        $view=new ViewModel();
       // $slug=$this->params('slug');
        $cate_id=$this->params('cate_id');
        $rand=$this->params('rand',0);
        $limit=$this->params('limit',0);
       // echo $cate_id.'--'.$rand;
        $redis_cate_related='_CACHE:RELATED:TYPE:0:CATE:MUSIC:RAND:'.$rand.':LIMIT:'.$limit.':cate_id:'.$cate_id;
        $redis=new Redis();
        $cateRelated_arr=$redis->_Get($redis_cate_related);
        if(!$cateRelated_arr||empty($cateRelated_arr))
        {
             $cateRelated= $this->getTbMusicTitleForead()->getMediaRelated(array('cate_id'=>$cate_id,'type'=>0),10);
             foreach($cateRelated as $value)
             {
                    if($value)
                    {
                         $cateRelated_arr[]=get_object_vars($value);
                    }
                                  
             } 
             $redis->_Set($redis_cate_related,$cateRelated_arr,7200);
             //   echo 'db-cate-related</br>';
        }else
        {
           // echo 'rd-cate-related</br>';
        }//var_dump($cateRelated_arr);
        $view->cateRelated=$cateRelated_arr;
        return $view;
        
    }
    public function videorelatedAction()
    {
        $view=new ViewModel();
       // $slug=$this->params('slug');
        $cate_id=$this->params('cate_id');
        $rand=$this->params('rand',0);
        $limit=$this->params('limit',0);
        //echo $cate_id.'--'.$rand;
        $redis_cate_related='_CACHE:RELATED:TYPE:0:CATE:VIDEO:RAND:'.$rand.':LIMIT:'.$limit.':cate_id:'.$cate_id;
        $redis=new Redis();
        $cateRelated_arr=$redis->_Get($redis_cate_related);
        if(!$cateRelated_arr||empty($cateRelated_arr))
        {
             $cateRelated= $this->getTbMusicTitleForead()->getMediaRelated(array('cate_id'=>$cate_id,'type'=>1),10);
             foreach($cateRelated as $value)
             {
                    if($value)
                    {
                         $cateRelated_arr[]=get_object_vars($value);
                    }
                                  
             } 
             $redis->_Set($redis_cate_related,$cateRelated_arr,7200);
              //  echo 'db-cate-related</br>';
        }else
        {
           // echo 'rd-cate-related</br>';
        }//var_dump($cateRelated_arr);
        $view->videoRelated=$cateRelated_arr;
        return $view;      
    }
    public function albumrelatedAction()
    {
        $view=new ViewModel();
      //  $slug=$this->params('slug');
        $cate_id=$this->params('cate_id');
        $rand=$this->params('rand',0);
        $limit=$this->params('limit',0);
       // echo $cate_id.'--'.$rand;
        $redis_cate_related='_CACHE:RELATED:TYPE:0:CATE:ALBUM:RAND:'.$rand.':LIMIT:'.$limit.':cate_id:'.$cate_id;
        $redis=new Redis();
        $cateRelated_arr=$redis->_Get($redis_cate_related);
        if(!$cateRelated_arr||empty($cateRelated_arr))
        {
             $cateRelated= $this->getTbPlaylist()->getRelatedPlaylist($cate_id,10);
             foreach($cateRelated as $value)
             {
                    if($value)
                    {
                         $cateRelated_arr[]=get_object_vars($value);
                    }
                                  
             } 
             $redis->_Set($redis_cate_related,$cateRelated_arr,7200);
               // echo 'db-cate-related</br>';
        }else
        {
           // echo 'rd-cate-related</br>';
        }//var_dump($cateRelated_arr);
        $view->albumRelated=$cateRelated_arr;
        return $view;      
    }
    public function musicartistrelatedAction()
    {
        $view=new ViewModel();
        //$slug=$this->params('slug');
        $artist_id=$this->params('artist_id');
        $rand=$this->params('rand',0);
        $limit=$this->params('limit',0);
        $redis=new Redis();
       // echo $artist_id.'--'.$rand;
        $redis_artist_related='_CACHE:RELATED:TYPE:0:CATE:MUSIC:RAND:'.$rand.':LIMIT:'.$limit.':ARTIST:artis_id_'.$artist_id;
        $artistRelated_arr=$redis->_Get($redis_artist_related);
        if(!$artistRelated_arr||empty($artistRelated_arr))
        {
            $artistRelated=$this->getTbMusicTitleForead()->getMediaRelated(array('artist_id'=>$artist_id,'type'=>0),$limit);
            foreach($artistRelated as $value)
             {
                    if($value)
                    {
                        $artistRelated_arr[]=get_object_vars($value); 
                    }
                                  
             } 
             $redis->_Set($redis_artist_related,$artistRelated_arr,7200);
              //  echo 'db-artist-related</br>';
        }
        else
        {
           // echo 'rd-artist-related</br>';
        }
        $view->music_artist_Related=$artistRelated_arr;
        return $view;      
    }
    public function videoartistrelatedAction()
    {
        $view=new ViewModel();
        //$slug=$this->params('slug');
        $artist_id=$this->params('artist_id');
        $rand=$this->params('rand',0);
        $limit=$this->params('limit',0);
        $redis=new Redis();
        //echo $artist_id.'--'.$rand;
        $redis_artist_related='_CACHE:RELATED:TYPE:0:CATE:VIDEO:RAND:'.$rand.':LIMIT:'.$limit.':ARTIST:artis_id_'.$artist_id;
        $artistRelated_arr=$redis->_Get($redis_artist_related);
        if(!$artistRelated_arr||empty($artistRelated_arr))
        {
            $artistRelated=$this->getTbMusicTitleForead()->getMediaRelated(array('artist_id'=>$artist_id,'type'=>1),$limit);
            foreach($artistRelated as $value)
             {
                    if($value)
                    {
                        $artistRelated_arr[]=get_object_vars($value); 
                    }
                                  
             } 
             $redis->_Set($redis_artist_related,$artistRelated_arr,7200);
            //    echo 'db-artist-related</br>';
        }
        else
        {
           // echo 'rd-artist-related</br>';
        }
        $view->video_artist_Related=$artistRelated_arr;
        return $view;      
    }
    
    //wiget cho page nghe si
    public function artistAction()
    {
        $view=new ViewModel();
       $artist_id=$this->params('artist_id',0);
       $view->artist=$this->getTbArtist()->getArtistDetailby_artist_id($artist_id);
       return $view;

    }
    public function usertopAction()
    {
        $view=new ViewModel();
        $user='';
         $validator = new Validator();
         if($validator->isValid())
         $user=$this->getTbUser()->getUserByUsername($validator->getUser());
         $view->user=$user;
         return $view;
         
    }
    public function bangxephangAction()
    {
        
    }
    
    
    /* start kiennt */
    public function artistFilterAction() {
    	$data = $this->params();
        $view = new ViewModel();
        $form = $this->getServiceLocator()->get('Cate\Form\ArtistFilterForm');
        
        $key = '_CACHE:WIDGET:ARTIST_FILTER';
        $redis=new Redis();
        $cache_data = $redis->_Get($key);
        if(!$cache_data || empty($cache_data)){
        	$data = $form->initData();
        	$redis->_Set($key, $data, 7200);
		}else{
			$form->initData($cache_data[0], $cache_data[1]);
		}
        $view->setVariable('artist_filter', $form);
        return $view;
    }
    
    public function artistRelatedAction() {
		$view = new ViewModel();
        $artist_id = $this->params('artist_id');
        $cate_id = $this->params('cate_id');
        
        if(!isset($cate_id)){
        	// auto cache
        	$artist_obj = $this->getTable('Artist\Model\Artist');
			$tmp = $artist_obj->getById($artist_id);
			$cate_id = $tmp['cate_id'];
        }
        if(!$tmp) return;
        $rand = $this->params('rand', 0);
        $limit = $this->params('limit', 0);
        
        $artist = $this->getTable('Artist\Model\Artist');
        $artist_related = $artist->getRelatedArtist($artist_id, $cate_id, 5);
        $view->artist_related = $artist_related;
        return $view;
    }
    
    /* end kiennt */
}